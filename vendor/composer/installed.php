<?php return array (
  'root' => 
  array (
    'pretty_version' => '2.4.5',
    'version' => '2.4.5.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'magento/project-community-edition',
  ),
  'versions' => 
  array (
    '2tvenom/cborencode' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '42aedccb861d01fc0554782348cc08f8ebf22332',
    ),
    'allure-framework/allure-codeception' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6156aef942a4e4de0add34a73d066a9458cefc6',
    ),
    'allure-framework/allure-php-api' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '50507f482d490f114054f2281cca487db47fa2bd',
    ),
    'allure-framework/allure-phpunit' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5584e7e4d7a232bbf7dd92d0cabf143147f72e9e',
    ),
    'astock/stock-api-libphp' => 
    array (
      'pretty_version' => '1.1.5',
      'version' => '1.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9e6460f43dbb2dbc6e442e017edb5427884d3bf',
    ),
    'aws/aws-crt-php' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3942776a8c99209908ee0b287746263725685732',
    ),
    'aws/aws-sdk-php' => 
    array (
      'pretty_version' => '3.235.5',
      'version' => '3.235.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b002b92369dbcb8d86f32546cb324eabcd19a4e',
    ),
    'bacon/bacon-qr-code' => 
    array (
      'pretty_version' => '2.0.7',
      'version' => '2.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd70c840f68657ce49094b8d91f9ee0cc07fbf66c',
    ),
    'beberlei/assert' => 
    array (
      'pretty_version' => 'v3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb70015c04be1baee6f5f5c953703347c0ac1655',
    ),
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.9.0',
      'version' => '4.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0bc8d1e30e96183e4f36db9dc79caead300beff4',
    ),
    'blueimp/jquery-file-upload' => 
    array (
      'replaced' => 
      array (
        0 => '5.6.14',
      ),
    ),
    'braintree/braintree_php' => 
    array (
      'pretty_version' => '6.5.1',
      'version' => '6.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b79ecd9ccde4ccf34b0c1f7343656ad5eece8e9c',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.9.3',
      'version' => '0.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca57d18f028f84f777b2168cd1911b0dee2343ae',
    ),
    'brick/varexporter' => 
    array (
      'pretty_version' => '0.3.7',
      'version' => '0.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e263cd718d242594c52963760fee2059fd5833c',
    ),
    'christian-riesen/base32' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2e82dab3baa008e24a505649b0d583c31d31e894',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.2.2',
      'version' => '4.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b88014f3348c93f3df99dc6d0967b0dbfa804474',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '184231d5eab66bc69afd6b9429344d80c67a33b6',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '59374f2fef0cabb9e8ddb53277e85cdca74328de',
    ),
    'codeception/module-sequence' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b75be26681ae90824cde8f8df785981f293667e1',
    ),
    'codeception/module-webdriver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'baa18b7bf70aa024012f967b5ce5021e1faa9151',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '9.0.9',
      'version' => '9.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '7439a53ae367986e9c22b2ac00f9d7376bb2f8cf',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '18a148dacd293fc7b044042f5aa63a82b08bff5d',
    ),
    'colinmollenhour/cache-backend-file' => 
    array (
      'pretty_version' => 'v1.4.5',
      'version' => '1.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '03c7d4c0f43b2de1b559a3527d18ff697d306544',
    ),
    'colinmollenhour/cache-backend-redis' => 
    array (
      'pretty_version' => '1.14.2',
      'version' => '1.14.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b042d26b8c2aa093485bdc4bb03a0113a03778d',
    ),
    'colinmollenhour/credis' => 
    array (
      'pretty_version' => 'v1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'afec8e58ec93d2291c127fa19709a048f28641e5',
    ),
    'colinmollenhour/php-redis-session-abstract' => 
    array (
      'pretty_version' => 'v1.4.6',
      'version' => '1.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f80354dc7c20caef29c881972a7966732c8c9bbd',
    ),
    'components/jquery' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.0',
      ),
    ),
    'components/jqueryui' => 
    array (
      'replaced' => 
      array (
        0 => '1.10.4',
      ),
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '30897edbfb15e784fe55587b4f73ceefd3c4d98c',
    ),
    'composer/composer' => 
    array (
      'pretty_version' => '2.2.18',
      'version' => '2.2.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '84175907664ca8b73f73f4883e67e886dfefb9f5',
    ),
    'composer/metadata-minifier' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c549d23829536f0d0e984aaabbf02af91f443207',
    ),
    'composer/pcre' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '67a32d7d6f9f560b726ab25a061b38ff3a80c560',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3953f23262f2bff1919fc82183ad9acb13ff62c9',
    ),
    'composer/spdx-licenses' => 
    array (
      'pretty_version' => '1.5.7',
      'version' => '1.5.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c848241796da2abf65837d51dce1fae55a960149',
    ),
    'composer/xdebug-handler' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e36aeed4616366d2b690bdce11f71e9178c579a',
    ),
    'container-interop/container-interop' => 
    array (
      'replaced' => 
      array (
        0 => '^1.2.0',
      ),
    ),
    'csharpru/vault-php' => 
    array (
      'pretty_version' => '4.3.1',
      'version' => '4.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '918bfffe85d3b290e1bf667b5f14e521fdc0063c',
    ),
    'dasprid/enum' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5abf82f213618696dda8e3bf6f64dd042d8542b2',
    ),
    'dealerdirect/phpcodesniffer-composer-installer' => 
    array (
      'pretty_version' => 'v0.7.2',
      'version' => '0.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c968e542d8843d7cd71de3c5c9c3ff3ad71a1db',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.3',
      'version' => '1.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '648b0343343565c4a056bfc8392201385e8d89f0',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '10dcfce151b967d20fde1b34ae6640712c3891bc',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
    ),
    'elasticsearch/elasticsearch' => 
    array (
      'pretty_version' => 'v7.17.0',
      'version' => '7.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1890f9d7fde076b5a3ddcf579a802af05b2e781b',
    ),
    'endroid/qr-code' => 
    array (
      'pretty_version' => '4.5.0',
      'version' => '4.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '36681470bd10352b53bcb9731bdf2270e0d79b22',
    ),
    'ezimuel/guzzlestreams' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abe3791d231167f14eb80d413420d1eab91163a8',
    ),
    'ezimuel/ringphp' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '92b8161404ab1ad84059ebed41d9f757e897ce74',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.14.0',
      'version' => '4.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ab42bd6e742c70c0a52f7b82477fcd44e64b75',
    ),
    'facebook/webdriver' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'fgrosse/phpasn1' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eef488991d53e58e60c9554b09b1201ca5ba9296',
    ),
    'friendsofphp/php-cs-fixer' => 
    array (
      'pretty_version' => 'v3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '47177af1cfb9dab5d1cc4daf91b7179c2efe7fad',
    ),
    'google/recaptcha' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '614f25a9038be4f3f2da7cbfd778dc5b357d2419',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.5.0',
      'version' => '7.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b50a2a1251152e43f6a37f0fa053e730a67d25ba',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.4.1',
      'version' => '2.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '69568e4293f4fa993f3b0e51c9723e1e17c41379',
    ),
    'guzzlehttp/ringphp' => 
    array (
      'replaced' => 
      array (
        0 => '1.2.0',
      ),
    ),
    'itonomy/productvisibilitygrid' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '319012f21b080764bd4cf63f547e43e4712d0873',
    ),
    'jms/metadata' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3a3214354b5a765a19875f7b7c5ebcd94e462e5',
    ),
    'jms/serializer' => 
    array (
      'pretty_version' => '3.18.1',
      'version' => '3.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '32956d3e3e1938f8130523a94297399d2b26fea7',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.2.12',
      'version' => '5.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad87d5a5ca981228e0e205c2bc7dfb8e24559b60',
    ),
    'laminas/laminas-captcha' => 
    array (
      'pretty_version' => '2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'debd6783ce593cb2e4cf74c3028baf1730918d85',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '4.5.2',
      'version' => '4.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da01fb74c08f37e20e7ae49f1e3ee09aa401ebad',
    ),
    'laminas/laminas-config' => 
    array (
      'pretty_version' => '3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e43d13dcfc273d4392812eb395ce636f73f34dfd',
    ),
    'laminas/laminas-db' => 
    array (
      'pretty_version' => '2.15.0',
      'version' => '2.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1125ef2e55108bdfcc1f0030d3a0f9b895e09606',
    ),
    'laminas/laminas-dependency-plugin' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '12737a58c1a73ea441c77a560b5d6d6ebd6d1503',
    ),
    'laminas/laminas-di' => 
    array (
      'pretty_version' => '3.9.1',
      'version' => '3.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b7091c50587f7ded53519d229421a02532feb9a',
    ),
    'laminas/laminas-diactoros' => 
    array (
      'pretty_version' => '2.17.0',
      'version' => '2.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b32597aa46b83c8b85bb1cf9a6ed4fe7dd980c5',
    ),
    'laminas/laminas-escaper' => 
    array (
      'pretty_version' => '2.10.0',
      'version' => '2.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '58af67282db37d24e584a837a94ee55b9c7552be',
    ),
    'laminas/laminas-eventmanager' => 
    array (
      'pretty_version' => '3.5.0',
      'version' => '3.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '41f7209428f37cab9573365e361f4078209aaafa',
    ),
    'laminas/laminas-feed' => 
    array (
      'pretty_version' => '2.18.2',
      'version' => '2.18.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a57fdb9df42950d5b7f052509fbdab0d081c6b6d',
    ),
    'laminas/laminas-http' => 
    array (
      'pretty_version' => '2.16.0',
      'version' => '2.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7300482d3e570e81b6a6ecf6b28da1b12334bf63',
    ),
    'laminas/laminas-json' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a0ce9f330b7d11e70c4acb44d67e8c4f03f437f',
    ),
    'laminas/laminas-loader' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0589ec9dd48365fd95ad10d1c906efd7711c16b',
    ),
    'laminas/laminas-mail' => 
    array (
      'pretty_version' => '2.16.0',
      'version' => '2.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ee1a384b96c8af29ecad9b3a7adc27a150ebc49',
    ),
    'laminas/laminas-mime' => 
    array (
      'pretty_version' => '2.10.0',
      'version' => '2.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '62a899a7c9100889c2d2386b1357003a2cb52fa9',
    ),
    'laminas/laminas-modulemanager' => 
    array (
      'pretty_version' => '2.12.0',
      'version' => '2.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd2dd3b3dc59e75a9f2117374222c0d84b25bf19',
    ),
    'laminas/laminas-mvc' => 
    array (
      'pretty_version' => '3.3.4',
      'version' => '3.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '46a3be585582788721c3533eb25226787cbd4359',
    ),
    'laminas/laminas-recaptcha' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3bdb2fcaf859b9f725f397dc1bc38b4a7696a71',
    ),
    'laminas/laminas-router' => 
    array (
      'pretty_version' => '3.9.0',
      'version' => '3.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebd084f7fda7520394b9ddc1e9ec2cbdf2094daa',
    ),
    'laminas/laminas-server' => 
    array (
      'pretty_version' => '2.11.1',
      'version' => '2.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f45e1a6f614a11af8eff5d2d409f12229101cfc1',
    ),
    'laminas/laminas-servicemanager' => 
    array (
      'pretty_version' => '3.16.0',
      'version' => '3.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '863c66733740cd36ebf5e700f4258ef2c68a2a24',
    ),
    'laminas/laminas-session' => 
    array (
      'pretty_version' => '2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f8a6077dd22b3b253583b1be84ddd5bf6fa1ef4',
    ),
    'laminas/laminas-soap' => 
    array (
      'pretty_version' => '2.10.0',
      'version' => '2.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b1245a09b523485060407f73a0058fb871d2c656',
    ),
    'laminas/laminas-stdlib' => 
    array (
      'pretty_version' => '3.13.0',
      'version' => '3.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '66a6d03c381f6c9f1dd988bf8244f9afb9380d76',
    ),
    'laminas/laminas-text' => 
    array (
      'pretty_version' => '2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8879e75d03e09b0d6787e6680cfa255afd4645a7',
    ),
    'laminas/laminas-uri' => 
    array (
      'pretty_version' => '2.9.1',
      'version' => '2.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e837dc15c8fd3949df7d1213246fd7c8640032b',
    ),
    'laminas/laminas-validator' => 
    array (
      'pretty_version' => '2.23.0',
      'version' => '2.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d61b6cc3b222f13807a18d9247cdfb084958b03',
    ),
    'laminas/laminas-view' => 
    array (
      'pretty_version' => '2.22.1',
      'version' => '2.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd4f49fccdc45ce9c39ec03b533d86f0ace62345',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e112dd2c099f4f6142c16fc65fda89a638e06885',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '2.4.5',
      'version' => '2.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9392c5f1df57d865c406ee65e5012d566686be12',
    ),
    'league/flysystem-aws-s3-v3' => 
    array (
      'pretty_version' => '2.4.3',
      'version' => '2.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf8c03f9c1c8a69f7fd2854d57127840e1b6ccd2',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
    ),
    'lusitanian/oauth' => 
    array (
      'pretty_version' => 'v0.8.11',
      'version' => '0.8.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc11a53db4b66da555a6a11fce294f574a8374f9',
    ),
    'magento-hackathon/magento-composer-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'magento/adobe-stock-integration' => 
    array (
      'pretty_version' => '2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/composer' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/composer-dependency-version-audit-plugin' => 
    array (
      'pretty_version' => '0.1.1',
      'version' => '0.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/composer-root-update-plugin' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework' => 
    array (
      'pretty_version' => '103.0.5',
      'version' => '103.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework-amqp' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework-bulk' => 
    array (
      'pretty_version' => '101.0.1',
      'version' => '101.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/framework-message-queue' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/google-shopping-ads' => 
    array (
      'pretty_version' => '4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/inventory-composer-installer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/inventory-metapackage' => 
    array (
      'pretty_version' => '1.2.5',
      'version' => '1.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-de_de' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-en_us' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-es_es' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-fr_fr' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-nl_nl' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-pt_br' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/language-zh_hans_cn' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/magento-coding-standard' => 
    array (
      'pretty_version' => '25',
      'version' => '25.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7be8305949f6683ff08534fbc22e5d42a1c4eba7',
    ),
    'magento/magento-composer-installer' => 
    array (
      'pretty_version' => '0.3.0',
      'version' => '0.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0c1987b1ba4c8bacde15cad86f4dace1e3957104',
    ),
    'magento/magento2-base' => 
    array (
      'pretty_version' => '2.4.5',
      'version' => '2.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/magento2-functional-testing-framework' => 
    array (
      'pretty_version' => '3.10.2',
      'version' => '3.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '919b2c3d10fb94ce4949eaaf1f2cc15fa67dd2f2',
    ),
    'magento/module-admin-adobe-ims' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-admin-analytics' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-admin-notification' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-ims' => 
    array (
      'pretty_version' => '2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-ims-api' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-admin-ui' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-asset' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-asset-api' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-client' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-client-api' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-image' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-image-admin-ui' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-adobe-stock-image-api' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-advanced-pricing-import-export' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-advanced-search' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-amqp' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-analytics' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-asynchronous-operations' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-authorization' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-aws-s3' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-aws-s3-page-builder' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-backend' => 
    array (
      'pretty_version' => '102.0.5',
      'version' => '102.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-backup' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-bundle' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-bundle-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-bundle-import-export' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cache-invalidate' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-captcha' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cardinal-commerce' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog' => 
    array (
      'pretty_version' => '104.0.5',
      'version' => '104.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-analytics' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-cms-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-customer-graph-ql' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-import-export' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-inventory' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-inventory-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-page-builder-analytics' => 
    array (
      'pretty_version' => '1.6.2',
      'version' => '1.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-rule' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-rule-configurable' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-rule-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-search' => 
    array (
      'pretty_version' => '102.0.5',
      'version' => '102.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-url-rewrite' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-url-rewrite-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-catalog-widget' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-checkout' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-checkout-agreements' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-checkout-agreements-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms' => 
    array (
      'pretty_version' => '104.0.5',
      'version' => '104.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-page-builder-analytics' => 
    array (
      'pretty_version' => '1.6.2',
      'version' => '1.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-url-rewrite' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cms-url-rewrite-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-compare-list-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-config' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-import-export' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-product' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-product-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-configurable-product-sales' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-contact' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cookie' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-cron' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-csp' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-currency-symbol' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer' => 
    array (
      'pretty_version' => '103.0.5',
      'version' => '103.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-analytics' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-downloadable-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-customer-import-export' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-deploy' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-developer' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-dhl' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-directory' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-directory-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-downloadable' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-downloadable-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-downloadable-import-export' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-eav' => 
    array (
      'pretty_version' => '102.1.5',
      'version' => '102.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-eav-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-elasticsearch' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-elasticsearch-6' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-elasticsearch-7' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-email' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-encryption-key' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-fedex' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-gift-message' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-gift-message-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-adwords' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-analytics' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-gtag' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-google-optimizer' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-graph-ql-cache' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-catalog-inventory' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-import-export' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-product' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-grouped-product-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-import-export' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-indexer' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-instant-purchase' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-integration' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-admin-ui' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-advanced-checkout' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-api' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-bundle-import-export' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-bundle-product' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-bundle-product-admin-ui' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-bundle-product-indexer' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-cache' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-admin-ui' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-api' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-frontend-ui' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-search' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-search-bundle-product' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-catalog-search-configurable-product' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product-admin-ui' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product-frontend-ui' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configurable-product-indexer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configuration' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-configuration-api' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-distance-based-source-selection' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-distance-based-source-selection-admin-ui' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-distance-based-source-selection-api' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-elasticsearch' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-export-stock' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-export-stock-api' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-graph-ql' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-grouped-product' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-grouped-product-admin-ui' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-grouped-product-indexer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-import-export' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-admin-ui' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-api' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-frontend' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-graph-ql' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-multishipping' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-quote' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-quote-graph-ql' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-sales' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-sales-admin-ui' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-sales-api' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-shipping' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-shipping-admin-ui' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-shipping-api' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-in-store-pickup-webapi-extension' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-indexer' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-low-quantity-notification' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-low-quantity-notification-admin-ui' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-low-quantity-notification-api' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-multi-dimensional-indexer-api' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-product-alert' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-quote-graph-ql' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-requisition-list' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-reservation-cli' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-reservations' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-reservations-api' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales-admin-ui' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales-api' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-sales-frontend-ui' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-setup-fixture-generator' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-shipping' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-shipping-admin-ui' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-source-deduction-api' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-source-selection' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-source-selection-api' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-swatches-frontend-ui' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-visual-merchandiser' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-inventory-wishlist' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-jwt-framework-adapter' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-jwt-user-token' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-layered-navigation' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-admin-ui' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-api' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-assistance' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-frontend-ui' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-log' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-page-cache' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-quote' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-login-as-customer-sales' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-marketplace' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-api' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-catalog' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-cms' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-synchronization' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-synchronization-api' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-synchronization-catalog' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-content-synchronization-cms' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-api' => 
    array (
      'pretty_version' => '101.0.4',
      'version' => '101.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-catalog' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-catalog-integration' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-catalog-ui' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-cms-ui' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-integration' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-metadata' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-metadata-api' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-renditions' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-renditions-api' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-synchronization' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-synchronization-api' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-synchronization-metadata' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-ui' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-gallery-ui-api' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-media-storage' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-message-queue' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-msrp' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-msrp-configurable-product' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-msrp-grouped-product' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-multishipping' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-mysql-mq' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-new-relic-reporting' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-newsletter' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-newsletter-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-offline-payments' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-offline-shipping' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-page-builder' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-page-builder-admin-analytics' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-page-builder-analytics' => 
    array (
      'pretty_version' => '1.6.2',
      'version' => '1.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-page-cache' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-payment' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-payment-graph-ql' => 
    array (
      'pretty_version' => '100.4.0',
      'version' => '100.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal' => 
    array (
      'pretty_version' => '101.0.5',
      'version' => '101.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal-captcha' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-paypal-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-persistent' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-product-alert' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-product-video' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-analytics' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-bundle-options' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-configurable-options' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-downloadable-links' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-quote-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-admin-ui' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-checkout' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-checkout-sales-rule' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-contact' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-customer' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-frontend-ui' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-migration' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-newsletter' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-paypal' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-review' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-send-friend' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-store-pickup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-ui' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-user' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-validation' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-validation-api' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-version-2-checkbox' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-version-2-invisible' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-version-3-invisible' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-webapi-api' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-webapi-graph-ql' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-webapi-rest' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-re-captcha-webapi-ui' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-related-product-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-release-notification' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-remote-storage' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-reports' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-require-js' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-review' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-review-analytics' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-review-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-robots' => 
    array (
      'pretty_version' => '101.1.1',
      'version' => '101.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-rss' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-rule' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales' => 
    array (
      'pretty_version' => '103.0.5',
      'version' => '103.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-analytics' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-inventory' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-rule' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sales-sequence' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sample-data' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-search' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-security' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-securitytxt' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-send-friend' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-send-friend-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-shipping' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-sitemap' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-store' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-store-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swagger' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swagger-webapi' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swagger-webapi-async' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swatches' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swatches-graph-ql' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-swatches-layered-navigation' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tax' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tax-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-tax-import-export' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-theme' => 
    array (
      'pretty_version' => '101.1.5',
      'version' => '101.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-theme-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-translation' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-two-factor-auth' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-ui' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-ups' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-url-rewrite' => 
    array (
      'pretty_version' => '102.0.4',
      'version' => '102.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-url-rewrite-graph-ql' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-user' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-usps' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-variable' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-vault' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-vault-graph-ql' => 
    array (
      'pretty_version' => '100.4.1',
      'version' => '100.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-version' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-webapi' => 
    array (
      'pretty_version' => '100.4.4',
      'version' => '100.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-webapi-async' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-webapi-security' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-weee' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-weee-graph-ql' => 
    array (
      'pretty_version' => '100.4.2',
      'version' => '100.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-widget' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-wishlist' => 
    array (
      'pretty_version' => '101.2.5',
      'version' => '101.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-wishlist-analytics' => 
    array (
      'pretty_version' => '100.4.3',
      'version' => '100.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/module-wishlist-graph-ql' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/page-builder' => 
    array (
      'pretty_version' => '1.7.2',
      'version' => '1.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/product-community-edition' => 
    array (
      'pretty_version' => '2.4.5',
      'version' => '2.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/project-community-edition' => 
    array (
      'pretty_version' => '2.4.5',
      'version' => '2.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/security-package' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/theme-adminhtml-backend' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/theme-frontend-blank' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/theme-frontend-luma' => 
    array (
      'pretty_version' => '100.4.5',
      'version' => '100.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'magento/zendframework1' => 
    array (
      'pretty_version' => '1.15.1',
      'version' => '1.15.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2381396d2a9a528be2f367b5ce2dddf650eac1d0',
    ),
    'mageplaza/magento-2-product-slider' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '719f98ad3af6ad0e8d7eca3a441b16ce1940c620',
    ),
    'mageplaza/module-core' => 
    array (
      'pretty_version' => '1.4.12',
      'version' => '1.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '85d1074fdee1259d438618c977ba6e21b76c4f39',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '720488632c590286b88b80e62aa3d3d551ad4a50',
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
    ),
    'mustache/mustache' => 
    array (
      'pretty_version' => 'v2.14.2',
      'version' => '2.14.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e62b7c3849d22ec55f3ec425507bf7968193a6cb',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '14daed4296fae74d9e3201d2c4925d1acb7aa614',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.15.1',
      'version' => '4.15.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ef6c55a3f47f89d7a374e6f835197a0b5fcf900',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.6.3',
      'version' => '2.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '58c3f47f650c94ec05a151692652a868995d2938',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'paypal/module-braintree' => 
    array (
      'pretty_version' => '4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'paypal/module-braintree-core' => 
    array (
      'pretty_version' => '4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'paypal/module-braintree-graph-ql' => 
    array (
      'pretty_version' => '4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'pdepend/pdepend' => 
    array (
      'pretty_version' => '2.10.3',
      'version' => '2.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da3166a06b4a89915920a42444f707122a1584c9',
    ),
    'pelago/emogrifier' => 
    array (
      'pretty_version' => 'v6.0.0',
      'version' => '6.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa72d5407efac118f3896bcb995a2cba793df0ae',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
    ),
    'php-amqplib/php-amqplib' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0bec5b392428e0ac3b3f34fbc4e02d706995833e',
    ),
    'php-cs-fixer/diff' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '29dc0d507e838c4580d018bd8b5cb412474f7ec3',
    ),
    'php-webdriver/webdriver' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b27ddf458d273c7d4602106fcaf978aa0b7fe15a',
    ),
    'phpcompatibility/php-compatibility' => 
    array (
      'pretty_version' => '9.3.5',
      'version' => '9.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9fb324479acf6f39452e0655d2429cc0d3914243',
    ),
    'phpgt/cssxpath' => 
    array (
      'pretty_version' => 'v1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f073ba346c49a339a7b2cda9ccfdb1994c5d271',
    ),
    'phpgt/dom' => 
    array (
      'pretty_version' => 'v2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '35962a3fa3f30b1a00d22f64c787881e2bf2e0e2',
    ),
    'phpmd/phpmd' => 
    array (
      'pretty_version' => '2.12.0',
      'version' => '2.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0b678ba71902f539c27c14332aa0ddcf14388ec',
    ),
    'phpseclib/mcrypt_compat' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a9f9f05b25fedce2ded16fa6008c1a6e4290603',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '3.0.16',
      'version' => '3.0.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '7181378909ed8890be4db53d289faac5b77f8b05',
    ),
    'phpstan/phpdoc-parser' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8dd908dd6156e974b9a0f8bb4cd5ad0707830f04',
    ),
    'phpstan/phpstan' => 
    array (
      'pretty_version' => '1.8.5',
      'version' => '1.8.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6598a5ff12ca4499a836815e08b4d77a2ddeb20',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '9.2.17',
      'version' => '9.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa94dc41e8661fe90c7316849907cba3007b10d8',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf1c2e7c203ac650e352f4cc675a7021e7d1b3cf',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.5.24',
      'version' => '9.5.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0aa6097bef9fd42458a9b3c49da32c6ce6129c5',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.0',
        1 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
        1 => '1.0|2.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cccc74ee5e328031b15640b51056ee8d3bb66c0a',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc9bb7fb5388691fd7373cd44dcb4d63bbcf24df',
    ),
    'react/promise' => 
    array (
      'pretty_version' => 'v2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '234f8fd1023c9158e2314fa9d7d0e6a83db42910',
    ),
    'rector/rector' => 
    array (
      'pretty_version' => '0.13.10',
      'version' => '0.13.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1e069db8ad3b4aea2b968248370c21415e4c180',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.2.3',
      ),
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.4.0',
      'version' => '8.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e41d2140031d533348b2192a83f02d8dd8a71d30',
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '4.0.6',
      'version' => '4.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f4261989e546dc112258c7a75935a81a7ce382',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3461e3fccc7cfdfc2720be910d3bd73c69be590d',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '5.1.4',
      'version' => '5.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b5dff7bb151a4db11d49d90e5408e4e938270f7',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '65e8b7db476c5dd267e65eea9cab77584d3cfff9',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '5.0.5',
      'version' => '5.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ca8db5a5fc9c8646244e629625ac486fa286bf2',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
    ),
    'sebastian/phpcpd' => 
    array (
      'pretty_version' => '6.0.3',
      'version' => '6.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3683aa0db2e8e09287c2bb33a595b2873ea9176',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd9d8cf3c5804de4341c283ed787f099f5506172',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb44e1cc6e557418387ad815780360057e40753e',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4211420d25eba80712bff236a98960ef68b866b7',
    ),
    'seld/phar-utils' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea2f4014f163c1be4c601b9b7bd6af81ba8d701c',
    ),
    'spomky-labs/aes-key-wrap' => 
    array (
      'pretty_version' => 'v6.0.0',
      'version' => '6.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '97388255a37ad6fb1ed332d07e61fa2b7bb62e0d',
    ),
    'spomky-labs/base64url' => 
    array (
      'pretty_version' => 'v2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '7752ce931ec285da4ed1f4c5aa27e45e097be61d',
    ),
    'spomky-labs/otphp' => 
    array (
      'pretty_version' => 'v10.0.3',
      'version' => '10.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9784d9f7c790eed26e102d6c78f12c754036c366',
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '3.6.2',
      'version' => '3.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e4e71592f69da17871dba6e80dd51bce74a351a',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v5.4.11',
      'version' => '5.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec79e03125c1d2477e43dde8528535d90cc78379',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.45',
      'version' => '4.4.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '28b77970939500fb04180166a1f716e75a871ef8',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.4.11',
      'version' => '5.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1681789f059ab756001052164726ae88512ae3d',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a692492190773c5310bc7877cb590c04c2f05be',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v5.4.11',
      'version' => '5.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8b9251016e9476db73e25fa836904bc0bf74c62',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '83a2310904a4f5d4f42526227b5a578ac82232a9',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be731658121ef2d8be88f3a1ec938148a9237291',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e866e9e5c1b22168e0ce5f0b467f19bba61266a',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.13',
      'version' => '1.1.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d5cd762abaa6b2a4169d3e77610193a7157129e',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d67c1f9a1937406a9be3171b4b22250c0a11447',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.4.11',
      'version' => '5.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '7872a66f57caffa2916a584db1aa7f12adc76f8c',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba6a9f0e8f3edd190520ee3b9a958596b6ca2e70',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4bfe9611b113b15d98a43da68ec9b5a00d56791',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.45',
      'version' => '4.4.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f2d38e9a3c6997ea0886ede5aaf337dfd0fc938',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '03876e9c5a36f5b45e7d9a381edda5421eff8a90',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v5.4.11',
      'version' => '5.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '54f14e36aa73cb8f7261d7686691fd4d75ea2690',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6fd1b9a79f6e3cf65f9e679b23af304cd9e010d4',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '59a8d271f00dd0e4c2e518104cc7963f655a1aa8',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '219aa369ceff116e673852dce47c3a41794c14bd',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9344f9cb97f3b19424af1a21a3b0e75b0a7d8d7e',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf44a9fd41feaac72b074de600314a93e2ae78e2',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e440d35fa0286f77fb45b79a03fedbeda9307e85',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfa0ae98841b9e461207c13ab093d76b0fa7bace',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '13f6d1271c663dc5ae9fb843a8f16521db7687a1',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cee9cdc4f7805e2699d9fd66991a0e6df8252a2',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d04b5c24f3c9a1a168a131f6cbe297155bc0d30',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.4.11',
      'version' => '5.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8f306d7b8ef34fb3db3305be97ba8e088fb4861',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.3.14',
      'version' => '5.3.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c441e9d2e340642ac8b951b753dea962d55b669d',
    ),
    'tedivm/jshrink' => 
    array (
      'pretty_version' => 'v1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0513ba1407b1f235518a939455855e6952a48bbc',
    ),
    'temando/module-shipping' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'temando/module-shipping-m2' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'temando/module-shipping-remover' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'thecodingmachine/safe' => 
    array (
      'pretty_version' => 'v1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8ab0876305a4cdaef31b2350fcb9811b5608dbc',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
    ),
    'tinymce/tinymce' => 
    array (
      'replaced' => 
      array (
        0 => '3.4.7',
      ),
    ),
    'trentrichardson/jquery-timepicker-addon' => 
    array (
      'replaced' => 
      array (
        0 => '1.4.3',
      ),
    ),
    'tubalmartin/cssmin' => 
    array (
      'pretty_version' => 'v4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3cbf557f4079d83a06f9c3ff9b957c022d7805cf',
    ),
    'twbs/bootstrap' => 
    array (
      'replaced' => 
      array (
        0 => '3.1.0',
      ),
    ),
    'videlalvaro/php-amqplib' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'web-token/encryption-pack' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-checker' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-console' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-core' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-easy' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-aescbc' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-aesgcm' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-aesgcmkw' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-aeskw' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-dir' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-ecdh-es' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-experimental' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-pbes2' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-encryption-algorithm-rsa' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-framework' => 
    array (
      'pretty_version' => 'v2.2.11',
      'version' => '2.2.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '643cced197e32471418bd89e7a44b69fd04eb9de',
    ),
    'web-token/jwt-key-mgmt' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-nested-token' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature-algorithm-ecdsa' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature-algorithm-eddsa' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature-algorithm-experimental' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature-algorithm-hmac' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature-algorithm-none' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-signature-algorithm-rsa' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/jwt-util-ecc' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'web-token/signature-pack' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.2.11',
      ),
    ),
    'webimpress/safe-writer' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d37cc8bee20f7cb2f58f6e23e05097eab5072e6',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
    ),
    'webonyx/graphql-php' => 
    array (
      'pretty_version' => 'v14.11.6',
      'version' => '14.11.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '6070542725b61fc7d0654a8a9855303e5e157434',
    ),
    'weew/helpers-array' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bff63111f9765b4277750db8d276d92b3e16ed0',
    ),
    'wikimedia/less.php' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a486d78b9bd16b72f237fc6093aa56d69ce8bd13',
    ),
    'zendframework/zend-server' => 
    array (
      'replaced' => 
      array (
        0 => '^2.8.1',
      ),
    ),
  ),
);

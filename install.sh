bin/magento setup:install \
--base-url=http://localhost/local.magento/ \
--db-host=localhost \
--db-name=magento \
--db-user=root \
--admin-firstname=admin \
--admin-lastname=satu \
--admin-email=atompubg13@gmail.com \
--admin-user=admin \
--admin-password=admin123 \
--language=en_US \
--currency=IDR \
--timezone=Asia/Jakarta \
--use-rewrites=1 
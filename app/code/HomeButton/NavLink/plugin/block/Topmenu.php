<?php

namespace HomeButton\NavLink\Plugin\Block;

use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Store\Model\StoreManagerInterface;

class Topmenu
{

    protected $nodeFactory;
    protected $storeManager;

    public function __construct(
        NodeFactory $nodeFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->nodeFactory = $nodeFactory;
        $this->storeManager = $storeManager;
    }

    public function beforeGetHtml(
        \Magento\Theme\Block\Html\Topmenu $subject,
        $outermostClass = '',
        $childrenWrapClass = '',
        $limit = 0
    ) {
        $node = $this->nodeFactory->create(
            [
                'data' => $this->getNodeAsArray(),
                'idField' => 'id',
                'tree' => $subject->getMenu()->getTree()
            ]
        );
        $subject->getMenu()->addChild($node);
    }

    /**
     *
     * Build node
     **/
    protected function getNodeAsArray()
    {
        $storeUrl = $this->storeManager->getStore()->getBaseUrl();
        return [
            'name' => __('HOME'),
            'id' => 'home',
            'url' => $storeUrl.'lime_helloword/page/index',
            'has_active' => true,
            'is_active' => true
        ];
    }
}
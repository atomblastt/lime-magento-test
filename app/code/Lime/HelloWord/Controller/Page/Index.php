<?php

namespace Lime\HelloWord\Controller\Page;

// use \Magento\Framework\App\ObjectManager;

class Index extends \Magento\Framework\App\Action\Action {
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $request = $objectManager->get('Magento\Framework\App\Request\Http');
        // echo "Hello Word";
        echo nl2br ("Hello World \n <a href='".$request->getServer('HTTP_REFERER')."'>Back</a>");
    }
    protected function _isAllowed() {
          return true;    
    }
}